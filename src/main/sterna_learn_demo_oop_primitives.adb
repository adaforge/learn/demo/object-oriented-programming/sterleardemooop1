--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright AdaCore (@adacore)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-01
--  --------------------------------------------------------------------------------------
--
--  @summary: Object Oriented Programming illustrations
--
--  @description: Derived types
--  https://learn.adacore.com/courses/intro-to-ada/chapters/object_oriented_programming.html
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

with Ada.Text_IO; use Ada.Text_IO;

procedure Sterna_Learn_Demo_OOP_Primitives is
  package Week is
    type Days is (Monday, Tuesday, Wednesday,
                  Thursday, Friday,
                  Saturday, Sunday);

     --  Print_Day is a primitive
     --  of the type Days
    procedure Print_Day (D : Days);
  end Week;

  package body Week is
    procedure Print_Day (D : Days) is
    begin
       Put_Line (Days'Image (D));
    end Print_Day;
  end Week;

  use Week;
  -- type Weekend_Days is new Days range Saturday .. Sunday;
  subtype Weekend_Days is Days range Saturday .. Sunday;

  --  A procedure Print_Day is automatically
  --  inherited here. It is as if the procedure
  --
  --  procedure Print_Day (D : Weekend_Days);
  --
  --  has been declared with the same body

  Sat : constant Weekend_Days := Saturday;
begin
   Print_Day (Sat);
end Sterna_Learn_Demo_OOP_Primitives;
