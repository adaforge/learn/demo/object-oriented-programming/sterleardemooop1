separate (Sterna_Learn_Demo_OOP1)
procedure Classical_Ada is
   -- ---------------------- --
   -- Classical Ada notation --
   -- ---------------------- --

begin
   New_Line;
   Put_Line ("----------------------");
   Put_Line ("Classical Ada notation");
   Put_Line ("----------------------");

   -- --------------
   -- Object_Oriented :
   -- --------------
   Put_Line ("======================");
   Put_Line ("=  Object Oriented   =");
   Put_Line ("======================");

   ------------------------------------------------------------------------
   -- 'Initialize' is called directly by Ada.Finalization before 'begin' --
   ------------------------------------------------------------------------
   --  Initialize (myGrand);
   --  Initialize (myParent);
   --  Initialize (myChild);

   ----------------------------
   -- Getting 'version' info --
   ----------------------------
   Put_Line ("myGrand is initialized");
   Put_Line ("   Tag name " & Ada.Tags.Wide_Expanded_Name (Grand.Object'Tag));
   Put_Line ("   Static version v" & Get_Static_Version (myGrand));    -- using overriden method
   Put_Line ("   Object version v" & Get_Version (myGrand));       -- Calling Base'Class Root
   New_Line;

   Put_Line ("myParent is initialized");
   Put_Line ("   Tag name " & Ada.Tags.Wide_Expanded_Name (Parent.Object'Tag));
   Put_Line ("   Static version v" & Get_Static_Version (myParent));    -- using overriden method
   Put_Line ("   Object version v" & Get_Version (myParent));      -- Calling Base'Class Root
   New_Line;

   Put_Line ("myChild is initialized");
   Put_Line ("   Tag name " & Ada.Tags.Wide_Expanded_Name (Child.Object'Tag));
   Put_Line ("   Static version v" & Get_Static_Version (myChild));    -- using overriden method
   Put_Line ("   Object version v" & Get_Version (myChild));       -- Calling Base'Class Root
   New_Line;

   -------------------
   -- calling 'Set' --
   -------------------
   -- Calling overriden method Set()
   Set (myGrand, "Grand");
   Set (myParent, "Parent");
   Set (myChild, "Child");

   -------------------
   -- calling 'Get' --
   -------------------
   -- Calling Base'Class defined at Grand.Get
   Put_Line ("Name is " & Get (myGrand));
   Put_Line ("Name is " & Get (myParent));
   Put_Line ("Name is " & Get (myChild));
   New_Line;

   -- -----------
   -- Any_Oriented :
   -- -----------
   New_Line;
   Put_Line ("=========================");
   Put_Line ("= Access type Oriented = ");
   Put_Line ("=========================");

   ------------------------------------------------------------------
   -- 'Initialize' is called directly by Ada.Finalization on 'new' --
   ------------------------------------------------------------------
   myGrand_Ref  := new Grand.Object;
   myParent_Ref := new Grand.Parent.Object;
   myChild_Ref  := new Grand.Parent.Child.Object;

   myFamily_Ref (1) := new Grand.Object;
   myFamily_Ref (2) := new Grand.Parent.Object;
   myFamily_Ref (3) := new Grand.Parent.Child.Object;

   --  Initialize (myGrand_Ref.all);
   --  Initialize (myParent_Ref.all);
   --  Initialize (myChild_Ref.all);

   ----------------------------
   -- Getting 'version' info --
   ----------------------------
   Put_Line ("myGrand is initialized");
   Put_Line ("   Static Version v" & Get_Static_Version (myGrand_Ref.All));    -- using overriden method
   Put_Line ("   Object version v" & Get_Version (myGrand_Ref.all));     -- Calling Base'Class Root
   New_Line;

   Put_Line ("myParent is initialized");
   Put_Line ("   Static Version v" & Get_Static_Version (myParent_Ref.All));    -- using overriden method
   Put_Line ("   Object version v" & Get_Version (myParent));     -- Calling Base'Class Root
   New_Line;

   Put_Line ("myChild is initialized");
   Put_Line ("   Static Version v" & Get_Static_Version (myChild_Ref.All));    -- using overriden method
   Put_Line ("   Object version v" & Get_Version (myChild));       -- Calling Base'Class Root
   New_Line;

   Put_Line ("Loop Init myFamily_Ref ...");
   for i in myFamily_Ref'Range loop
      Initialize (myFamily_Ref (i).All);
   end loop;

   Put_Line ("Loop ... Get_Versions");
   for i in myFamily_Ref'Range loop
      Put ("   ");
      Put_Line ("myFamily [" & i'Wide_Image & "] is initialized");
      Put ("      ");
      Put_Line ("   Tag name " & Ada.Tags.Wide_Expanded_Name (myFamily_Ref (i).All'Tag));
      Put ("      ");
      Put_Line ("   Static Version v" & Get_Static_Version (myFamily_Ref (i).All));    -- using overriden method
      Put ("      ");
      Put_Line ("   Object version v" & Get_Version (myFamily_Ref (i).All));     -- Calling Base'Class Root
   end loop;
   New_Line;

   -------------------
   -- calling 'Set' --
   -------------------
   Set (myGrand_Ref.all, "Grand");
   Set (myParent_Ref.all, "Parent");
   Set (myChild_Ref.all, "Child");

   Put_Line ("Loop ... Set()");
   for i in myFamily_Ref'Range loop
      Put ("   ");
      Put_Line ("Set Family [" & i'Wide_Image & "]' name ...");
      Put ("   ");
      Put ("   ");
      Put_Line ("=> ""Family name #" & i'Wide_Image & """");
      Grand.Set (
            This => myFamily_Ref (i).all, 
            Name => "Family name #" & i'Wide_Image);
   end loop;
   New_Line;

   -------------------
   -- calling 'Get' --
   -------------------
   Put_Line ("Name is " & Get (myGrand_Ref.all));
   Put_Line ("Name is " & Get (myParent_Ref.all));
   Put_Line ("Name is " & Get (myChild_Ref.all));
   New_Line;

   Put_Line ("Loop ... Get()");
   for i in myFamily_Ref'Range loop
      Put ("   ");
      Put_Line ("Family [" & i'Wide_Image & "] name is " & Grand.Get (myFamily_Ref (i).All));
   end loop;
   New_Line;

end Classical_Ada;
