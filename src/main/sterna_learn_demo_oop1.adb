--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-01
--  --------------------------------------------------------------------------------------
--
--  @summary: Object Oriented Programming illustrations
--
--  @description: inheritance, interfaces, constructors, dynamic dispatching, polymorphism
--                anonymous and named access types
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

with Sterna.Learn.Demo.OOP;
with Sterna.Learn.Demo.OOP.Activity;
with Sterna.Learn.Demo.OOP.Grand;
with Sterna.Learn.Demo.OOP.Grand.Parent;
with Sterna.Learn.Demo.OOP.Grand.Parent.Child;

-- ----------------------------------------------- --
-- Letting the compiler findout the method to call --
-- ----------------------------------------------- --
use Sterna.Learn.Demo.OOP;
use Sterna.Learn.Demo.OOP.Activity;
use Sterna.Learn.Demo.OOP.Grand;
use Sterna.Learn.Demo.OOP.Grand.Parent;
use Sterna.Learn.Demo.OOP.Grand.Parent.Child;
-- ----------------------------------------------- --

use Sterna.Learn.Demo;

with Ada.Wide_Text_IO; use Ada.Wide_Text_IO;

with Ada.Tags;

procedure Sterna_Learn_Demo_OOP1 is

   -- ------------------
   -- Object_Oriented --
   -- ------------------
   myGrand  : Grand.Object;
   myParent : Grand.Parent.Object;
   myChild  : Grand.Parent.Child.Object;

-- myFamily : array (1 .. 3) of Grand.Any_Object;
-- myFamily_Obj : array (1 .. 3) of access Grand.Object;

   -- ---------------
   -- Any_Oriented --
   -- ---------------
   -- Named anonymous objects
   myGrand_Ref  : Grand.Any_Object;
   myParent_Ref : Grand.Parent.Any_Object;
   myChild_Ref  : Grand.Parent.Child.Any_Object;

   -- Named access type
   myFamily_Ref : array (1 .. 3) of Grand.Any_Class;
   -- Anonymous access type
-- myFamily_Ref : array (1 .. 3) of access Grand.Object'Class;

   -- ------------------
   -- 2 coding styles --
   -- ------------------
   procedure Classical_Ada is separate;

   procedure Prefixed is separate;

begin

   Classical_Ada;

   New_Line;

   Prefixed;

end Sterna_Learn_Demo_OOP1;
