separate (Sterna_Learn_Demo_OOP1)
procedure Prefixed is
   -- -------------------- --
   -- OO prefixed notation --
   -- -------------------- --
   
begin
   New_Line;
   Put_Line ("--------------------");
   Put_Line ("OO prefixed notation");
   Put_Line ("--------------------");

   -- --------------
   -- Object_Oriented :
   -- --------------
   Put_Line ("======================");
   Put_Line ("=  Object Oriented   =");
   Put_Line ("======================");

   ------------------------------------------------------------------------
   -- 'Initialize' is called directly by Ada.Finalization before 'begin' --
   ------------------------------------------------------------------------
   --  myGrand.Initialize;
   --  myParent.Initialize;
   --  myChild.Initialize;

   ----------------------------
   -- Getting 'version' info --
   ----------------------------
   Put_Line ("myGrand is initialized");
   Put_Line ("   Tag name " & Ada.Tags.Wide_Expanded_Name (Grand.Object'Tag));
   Put_Line ("   Static version v" & myGrand.Get_Static_Version);    -- Calling overriden method
   Put_Line ("   Object version v" & myGrand.Get_Version);    -- Calling Base'Class Root
   New_Line;

   Put_Line ("myParent is initialized");
   Put_Line ("   Tag name " & Ada.Tags.Wide_Expanded_Name (Parent.Object'Tag));
   Put_Line ("   Static version v" & myParent.Get_Static_Version);    -- Calling overriden method
   Put_Line ("   Object version v" & myParent.Get_Version);    -- Calling Base'Class Root
   New_Line;

   Put_Line ("myChild is initialized");
   Put_Line ("   Tag name " & Ada.Tags.Wide_Expanded_Name (Child.Object'Tag));
   Put_Line ("   Static version v" & myChild.Get_Static_Version);    -- Calling overriden method
   Put_Line ("   Object version v" & myChild.Get_Version);    -- Calling Base'Class Root
   New_Line;

   -------------------
   -- calling 'Set' --
   -------------------
   -- Calling overriden method Set()
   myGrand.Set ("Grand ");
   myParent.Set ("Parent");
   myChild.Set ("Child ");

   -------------------
   -- calling 'Get' --
   -------------------
   -- Calling Base'Class defined at Grand.Get
   Put_Line ("Name is " & myGrand.Get);
   Put_Line ("Name is " & myParent.Get);
   Put_Line ("Name is " & myChild.Get);
   New_Line;

   ----------------------------
   -- calling 'Set_Activity' --
   ----------------------------
   -- Calling overriden method
   myGrand.Set_Activity (Retired);
   myParent.Set_Activity (Active);
   myChild.Set_Activity (Student);
   New_Line;

   -----------------------------
   -- calling 'Show_Activity' --
   -----------------------------
   Put_Line (myGrand.Get & " activity is " & myGrand.Show_Activity'Wide_Image);
   Put_Line (myParent.Get & " activity is " & myParent.Show_Activity'Wide_Image);
   Put_Line (myChild.Get & " activity is " & myChild.Show_Activity'Wide_Image);
   New_Line;

   ----------------------
   -- calling 'Credit' --
   ----------------------
   -- Calling overriden method
   myGrand.Credit (1_200.00);
   myParent.Credit (3_400.00);
   myChild.Credit (100.00);

   -----------------------------
   -- calling 'Show_Earnings' --
   -----------------------------
   -- Calling overriden method
   Put_Line (myGrand.Get & " earnings is" & myGrand.Show_Earnings'Wide_Image);
   Put_Line (myParent.Get & " earnings is" & myParent.Show_Earnings'Wide_Image);
   Put_Line (myChild.Get & " earnings is" & myChild.Show_Earnings'Wide_Image);

   -- -----------
   -- Any_Oriented :
   -- -----------
   New_Line;
   Put_Line ("=========================");
   Put_Line ("= Access type Oriented = ");
   Put_Line ("=========================");

   ------------------------------------------------------------------
   -- 'Initialize' is called directly by Ada.Finalization on 'new' --
   ------------------------------------------------------------------
   myGrand_Ref  := new Grand.Object;
   myParent_Ref := new Grand.Parent.Object;
   myChild_Ref  := new Grand.Parent.Child.Object;

   myFamily_Ref (1) := new Grand.Object;
   myFamily_Ref (2) := new Grand.Parent.Object;
   myFamily_Ref (3) := new Grand.Parent.Child.Object;

   --  myGrand_Ref.Initialize;
   --  myParent_Ref.Initialize;
   --  myChild_Ref.Initialize;

   ----------------------------
   -- Getting 'version' info --
   ----------------------------
   Put_Line ("myGrand_Ref is initialized");
   Put_Line ("   Static Version v" & myGrand_Ref.Get_Static_Version);    -- using overriden method
   Put_Line ("   Object version v" & myGrand_Ref.Get_Version);    -- Calling Base'Class Root
   New_Line;

   Put_Line ("myParent_Ref is initialized");
   Put_Line ("   Static Version v" & myParent_Ref.Get_Static_Version);    -- using overriden method
   Put_Line ("   Object version v" & myParent.Get_Version);    -- Calling Base'Class Root
   New_Line;

   Put_Line ("myChild_Ref is initialized");
   Put_Line ("   Static Version v" & myChild_Ref.Get_Static_Version);    -- using overriden method
   Put_Line ("   Object version v" & myChild.Get_Version);     -- Calling Base'Class Root
   New_Line;

   Put_Line ("Loop Init myFamily_Ref ...");
   for i in myFamily_Ref'Range loop
      myFamily_Ref (i).Initialize;
   end loop;

   Put_Line ("Loop ... Get_Versions");
   for i in myFamily_Ref'Range loop
      Put ("   ");
      Put_Line ("myFamily_Ref [" & i'Wide_Image & "] is initialized");
      Put ("      ");
      Put_Line ("Tag name " & Ada.Tags.Wide_Expanded_Name (myFamily_Ref (i)'Tag));
      Put ("      ");
      Put_Line ("Static Version v" & myFamily_Ref (i).Get_Static_Version);    -- using overriden method
      Put ("      ");
      Put_Line ("Object version v" & myFamily_Ref (i).Get_Version);     -- Calling Base'Class Root
   end loop;
   New_Line;

   -------------------
   -- calling 'Set' --
   -------------------
   myGrand_Ref.Set ("Grand ");
   myParent_Ref.Set ("Parent");
   myChild_Ref.Set ("Child");

   Put_Line ("Loop ... Set()");
   for i in myFamily_Ref'Range loop
      Put ("   ");
      Put_Line ("Set Family_Ref [" & i'Wide_Image & "]' name ...");
      Put ("   ");
      Put ("   ");
      Put_Line ("=> ""Family name #" & i'Wide_Image & """");
      myFamily_Ref (i).Set (
            Name => "Family name #" & i'Wide_Image);
   end loop;
   New_Line;

   -------------------
   -- calling 'Get' --
   -------------------
   Put_Line ("Name is " & myGrand_Ref.Get);
   Put_Line ("Name is " & myParent_Ref.Get);
   Put_Line ("Name is " & myChild_Ref.Get);
   New_Line;

   Put_Line ("Loop ... Get()");
   for i in myFamily_Ref'Range loop
      Put ("   ");
      Put_Line ("Family_Ref [" & i'Wide_Image & "] name is " & myFamily_Ref (i).Get);
   end loop;
   New_Line;

   ----------------------------------------
   -- Copy each person into myFamily_Ref --
   ----------------------------------------
   Put_Line ("Copy each person into myFamily_Ref ...");
   myFamily_Ref (1) := Grand.Any_Class (myGrand_Ref);
   myFamily_Ref (2) := Grand.Any_Class (myParent_Ref);
   myFamily_Ref (3) := Grand.Any_Class (myChild_Ref);
   New_Line;

   -------------------
   -- calling 'Get' --
   -------------------
   Put_Line ("Loop ... Get()");
   for i in myFamily_Ref'Range loop
      Put ("   ");
      Put_Line ("Family_Ref [" & i'Wide_Image & "] name is " & myFamily_Ref (i).Get);
   end loop;
   New_Line;

   ---------------------------------------
   -- calling 'Set_Activity' & 'Credit' --
   ---------------------------------------
   myGrand_Ref.Set_Activity (Retired);
   myParent_Ref.Set_Activity (Active);
   myChild_Ref.Set_Activity (Student);

   myGrand_Ref.Credit (1_200.00);
   myParent_Ref.Credit (3_400.00);
   myChild_Ref.Credit (100.00);

   -----------------------------------------------
   -- calling 'Show_Earnings' & 'Show_Earnings' --
   -----------------------------------------------
   Put_Line ("Loop ... Show_Activity() & Show_Earnings()");
   for i in myFamily_Ref'Range loop
      Put ("   ");
      Put (myFamily_Ref (i).Get
            & " activity is "
            & myFamily_Ref (i).Show_Activity'Wide_Image);
      Put (" and earnings is"
            & myFamily_Ref (i).Show_Earnings'Wide_Image);
      New_Line;
   end loop;

end Prefixed;
