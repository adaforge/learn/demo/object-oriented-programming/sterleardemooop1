--  pragma Wide_Character_Encoding (UTF8);
--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------

with Ada.Strings.Wide_Fixed;
use Ada.Strings;

package body Sterna.Learn.Demo.OOP is

   -- use Sterna.Learn.Demo.OOP.Names.Name_Strings;

   Implement_Version : constant Integer := 0;

   Static_Root : Root_Wide := (
         Version => (API_version, Implement_Version));

   ----------
   -- Init --
   ----------
   procedure Initialize is
   begin
      Static_Root.Version := (API       => 10 * API_Version,
                              Implement => 10 * Implement_Version);
   end Initialize;

   overriding
   procedure Initialize (This : in out Root) is
   begin
      This.Version := (API       => 100 * API_Version,
                       Implement => 100 * Implement_Version);
      Initialize;
   end Initialize;

   function To_Wide_String (Version : Version_Record) return Wide_String is
   begin
      return Wide_Fixed.Trim (Version.API'Wide_Image, Left) &
             "." &
             Wide_Fixed.Trim (Version.Implement'Wide_Image, Left);
   end To_Wide_String;

   -- ---------------------
   -- Get static Version --
   -- ---------------------
   -- pragma Warnings (Off, "function ""Get_Static_Version"" is not referenced");
   function Get_Static_Version return Wide_String is
   begin
      return To_Wide_String (Static_Root.Version);
   end Get_Static_Version;

   pragma Warnings (Off, "formal parameter ""This"" is not referenced");
   function Get_Static_Version (This : Root) return Wide_String is
   begin
      return Get_Static_Version;
   end Get_Static_Version;
   pragma Warnings (On);

   -- -----------------------
   -- Get instance Version --
   -- -----------------------
   function Get_Version (This : Root'Class) return Wide_String is
   begin
      return To_Wide_String (This.Version);
   end Get_Version;

end Sterna.Learn.Demo.OOP;
