--  pragma Wide_Character_Encoding (UTF8);
--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------

package body Sterna.Learn.Demo.OOP.Grand is

   use Name_Strings;

   Implement_Version : constant Integer := 10;

   Static_Object : Class_Wide := (
         Version => (API_version, Implement_Version),
         Default_Name => Default_Name_Value);

   ----------
   -- Init --
   ----------
   procedure Initialize is
   begin
      Static_Object.Version := (API       => 10 * API_Version,
                                Implement => 10 * Implement_Version);
      Sterna.Learn.Demo.OOP.Initialize;
   end Initialize;

   overriding
   procedure Initialize (This : in out Object) is
   begin
      This.Version := (API       => 100 * API_Version,
                       Implement => 100 * Implement_Version);
      Initialize;
   end Initialize;

   -- --------------
   -- Get_Version --
   -- --------------
   function Get_Static_Version return Wide_String is
   begin
      -- calling Sterna.Learn.Demo.OOP.Get_Static_Version (Version_Record)
      return To_Wide_String (Static_Object.Version);
   end Get_Static_Version;

   overriding
   function Get_Static_Version (This : Object) return Wide_String is
   begin
      pragma Warnings (Off, "formal parameter ""This"" is not referenced");
      return Get_Static_Version;
      pragma Warnings (On);
   end Get_Static_Version;

   ---------
   -- Get --
   ---------
   function Get (This : Class) return Wide_String is
   begin
      return To_Wide_String (This.Name);
   end Get;

   ---------
   -- Set --
   ---------
   procedure Set (This : in out Object; Name : Wide_String) is
   begin
      This.Name := To_Bounded_Wide_String ("Grand." & Name);
   end Set;

   ------------------------------------------
   -- from Activity and specific Interface --
   ------------------------------------------
   overriding
   procedure Set_activity (This : in out Object; Activity : Activity_Kind) is
   begin
      This.Activity := Activity;
   end Set_activity;

   function Show_activity (This : Class) return Activity_Kind is
   begin
      return This.Activity;
   end Show_activity;

   overriding
   procedure Credit (This : in out Object; Amount : Money) is
   begin
      This.Monthly_Pension := Amount;
   end Credit;

   overriding
   function Show_Earnings (This : Object) return Money is
   begin
      return This.Monthly_Pension;
   end Show_Earnings;

end Sterna.Learn.Demo.OOP.Grand;
