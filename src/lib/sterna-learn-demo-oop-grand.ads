--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-01
--  --------------------------------------------------------------------------------------
--
--  @summary: Object Oriented Programming illustrations
--
--  @description: inheritance, interfaces, constructors, dynamic dispatching, polymorphism
--                anonymous and named access types
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

with Sterna.Learn.Demo.OOP.Activity;
use Sterna.Learn.Demo.OOP.Activity;

with Sterna.Learn.Demo.OOP.Activity.Pension;
use Sterna.Learn.Demo.OOP.Activity.Pension;

with Sterna.Learn.Demo.OOP.Names;
use Sterna.Learn.Demo.OOP.Names;

package Sterna.Learn.Demo.OOP.Grand is
      pragma Preelaborate;

   -- Object Constants
   API_version : constant Integer := 1;
   Default_Name_Value : constant Wide_String := "What is your name ?";

-- and Version_Protocol

   -- Proxy declarations from outer package only here
   subtype Super_Object is Root;
   subtype Super_Class is Root'Class;
   subtype Super_Any is Any_Root;

   -- Object & Class definitions
   type Object is new Super_Object and Pension_Protocol with private;
   subtype Class is Object'Class;
   type Class_Wide is private; -- static in Java / Swift / ...

   -- Access definitions (if needed)
   type Any_Object is access all Object;
   type Any_Class is access all Class;

   ----------
   -- Init --
   ----------
   procedure Initialize;

   overriding
   procedure Initialize (This : in out Object);

   -- --------------
   -- Get_Version --
   -- --------------
   function Get_Static_Version return Wide_String;

   overriding
   function Get_Static_Version (This : Object) return Wide_String;

   -- function Get_Version (This : Object) return Wide_String;
   -- is defined and will be cajlled from 'Sterna.Learn.Demo.OOP'

  ---------
   -- Get --
   ---------
   function Get (This : Class) return Wide_String;

   ---------
   -- Set --
   ---------
   procedure Set (This : in out Object; Name : Wide_String);

   --------------------
   -- from Interface --
   --------------------
   overriding
   procedure Set_activity (This : in out Object; Activity : Activity_Kind);

   function Show_activity (This : Class) return Activity_Kind;

   overriding
   procedure Credit (This : in out Object; Amount : Money);

   overriding
   function Show_Earnings (This : Object) return Money;

private

   type Object is new Root and Pension_Protocol with record
      Name            : Name_String; -- Name_Strings.Bounded_Wide_String;
      Activity        : Activity_Kind;
      Monthly_Pension : Money;
   end record;

   type Class_Wide is record
      Version      : Version_Record;
      Default_Name : Wide_String (1 .. Default_Name_Value'Length);
   end record;

   subtype Dispatch is Super_Object;
   subtype Dispatch_Any is Super_Any;

end Sterna.Learn.Demo.OOP.Grand;
