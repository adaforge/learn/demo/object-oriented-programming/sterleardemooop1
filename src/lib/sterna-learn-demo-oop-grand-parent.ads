--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-01
--  --------------------------------------------------------------------------------------
--
--  @summary: Object Oriented Programming illustrations
--
--  @description: inheritance, interfaces, constructors, dynamic dispatching, polymorphism
--                anonymous and named access types
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

with Sterna.Learn.Demo.OOP.Activity.Salary;
use Sterna.Learn.Demo.OOP.Activity.Salary;

package Sterna.Learn.Demo.OOP.Grand.Parent is
      pragma Preelaborate;

   -- Object Constants
   API_version : constant Integer := 2;
   Default_Name_Value : constant Wide_String := "What is your name ?";

   -- Proxy declarations from outer package only here
   subtype Super_Object is Grand.Object;
   subtype Super_Class is Grand.Object'Class;
   subtype Super_Any is Grand.Any_Object;

   -- Object & Class definitions
   type Object is new Super_Object and Salary_Protocol with private;
   subtype Class is Object'Class;
   type Class_Wide is private; -- static in Java / Swift / ...

   -- Access definitions (if needed)
   type Any_Object is access all Object;
   type Any_Class is access Class;

   ----------
   -- Init --
   ----------
   procedure Initialize;

   overriding
   procedure Initialize (This : in out Object);

   -- --------------
   -- Get_Version --
   -- --------------
   function Get_Static_Version return Wide_String;

   overriding
   function Get_Static_Version (This : Object) return Wide_String;

   -- function Get_Version (This : Object) return Wide_String;
   -- is defined in 'Sterna.Learn.Demo.OOP'

   ---------
   -- Get --
   ---------
   -- function Get (This : Object) return Wide_String;
   -- is defined and will be called from 'Sterna.Learn.Demo.OOP.Grand'

   ---------
   -- Set --
   ---------
   overriding
   procedure Set (This : in out Object; Name : Wide_String);

   --------------------
   -- from Interface --
   --------------------
   overriding
   procedure Set_activity (This : in out Object; Activity : Activity_Kind);

   -- function Show_activity (This : Object'Class) return Activity_Kind;
   -- is defined and will be called from 'Sterna.Learn.Demo.OOP.Grand'

   overriding
   procedure Credit (This : in out Object; Amount : Money);

   overriding
   function Show_Earnings (This : Object) return Money;

private
   type Object is new Super_Object and Salary_Protocol with record
      Monthly_Salary : Money;
   end record;

   type Class_Wide is record
      Version      : Version_Record;
      Default_Name : Wide_String (1 .. Default_Name_Value'Length);
   end record;

   subtype Dispatch is Super_Object;
   subtype Dispatch_Any is Super_Any;

end Sterna.Learn.Demo.OOP.Grand.Parent;
