--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-01
--  --------------------------------------------------------------------------------------
--
--  @summary: Object Oriented Programming illustrations
--
--  @description: inheritance, interfaces, constructors, dynamic dispatching, polymorphism
--                anonymous and named access types
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

package Sterna.Learn.Demo.OOP.Activity is
   pragma Preelaborate;

   type Activity_Kind is (Student, Active, Retired);

   type Money is delta 0.01 digits 7;

   type Activity_Protocol is interface;
 
   procedure Set_activity (This : in out Activity_Protocol; Activity : Activity_Kind) is abstract;

   function Show_activity (This : Activity_Protocol'Class) return Activity_Kind is abstract;

end Sterna.Learn.Demo.OOP.Activity;