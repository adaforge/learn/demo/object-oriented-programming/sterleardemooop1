--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-01
--  --------------------------------------------------------------------------------------
--
--  @summary: Object Oriented Programming illustrations
--
--  @description: inheritance, interfaces, constructors, dynamic dispatching, polymorphism
--                anonymous and named access types
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

package Sterna.Learn.Demo.OOP.Activity.Pension is
   pragma Preelaborate;

   type Pension_Protocol is interface and Activity_Protocol;

   procedure Credit (This : in out Pension_Protocol; Amount : Money) is abstract;

   function Show_Earnings (This : Pension_Protocol) return Money is abstract;

end Sterna.Learn.Demo.OOP.Activity.Pension;