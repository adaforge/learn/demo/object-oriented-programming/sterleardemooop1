pragma Wide_Character_Encoding (UTF8);
--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-01
--  --------------------------------------------------------------------------------------
--
--  @summary: Object Oriented Programming illustrations
--
--  @description: inheritance, interfaces, constructors, dynamic dispatching, polymorphism
--                anonymous and named access types
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

with Ada.Finalization;

package Sterna.Learn.Demo.OOP is

   pragma Preelaborate;

   -- Object Constants
   API_version : constant Integer := 0;
   
   type Version_Record is record
      API       : Integer := 0;
      Implement : Integer := 0;
   end record;

   type Root_Wide is record
      Version : Version_Record;
   end record;

   -- Object & Class definitions
-- type Root is tagged record
   type Root is new Ada.Finalization.Controlled with record
      Version : Version_Record;
   end record;
   subtype Root_Class is Root'Class;

   -- Access definitions (if needed)
   type Any_Root is access Root;
   type Any_Class is access Root_Class;

   ----------
   -- Init --
   ----------
   overriding
   procedure Initialize (This : in out Root);
-- procedure Adjust     (This : in out Root);
-- procedure Finalize   (This : in out Root);

   procedure Initialize;

   -- --------------
   -- Get_Version --
   -- --------------
   function Get_Static_Version (This : Root) return Wide_String;
   function Get_Version (This : Root'Class) return Wide_String;

private

   function To_Wide_String (Version : Version_Record) return Wide_String;

end Sterna.Learn.Demo.OOP;
