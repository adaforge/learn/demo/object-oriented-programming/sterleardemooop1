--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2023 STERNA MARINE sas (william.franck@sterna.io)
--  SPDX-Creator: William J. Franck (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-03-01
--  --------------------------------------------------------------------------------------
--
--  @summary: Object Oriented Programming illustrations
--
--  @description: inheritance, interfaces, constructors, dynamic dispatching, polymorphism
--                anonymous and named access types
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

with Ada.Strings.Wide_Bounded;

package Sterna.Learn.Demo.OOP.Names is
   pragma Preelaborate;

   package Name_Strings is new Ada.Strings.Wide_Bounded.Generic_Bounded_Length (50);

   subtype Name_String is Name_Strings.Bounded_Wide_String;

end Sterna.Learn.Demo.OOP.Names;