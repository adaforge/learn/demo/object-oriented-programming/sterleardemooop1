# Object Oriented Programming illustrations

## Design

See UML description [OO_Demo.pdf](design/OO_Demo.pdf)

## What it shows

* inheritance
* interfaces
* constructors
* dynamic dispatching
* polymorphism
* anonymous and named access types

* Derived types
